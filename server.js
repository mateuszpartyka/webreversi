"use strict";
const app = require("express")();
const http = require("http").Server(app);
const port = process.env.PORT || 1337;
const io = require("socket.io")(http);
const path = require('path');

const Player = require('./scripts/server/Player.js');
const Room = require('./scripts/server/Room.js');
const TileColors = require('./scripts/server/TileColors.js');
const GameStates = require('./scripts/server/GameStates.js');

let rooms = [];
const defaultPlayerTime = 5 * 60 * 1000;

// callback to app root
app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

app.get("*", (req, res) => {
    res.sendFile(__dirname + req.path);
})

// listening to some port
http.listen(port, () => {
    console.log(`Listening to ${port}`);
});

io.on("connection", socket => {
    socket.emit("Socket.Connection.Successful", {
        Message: "Welcome to WebReversi!",
        DefaultUsername: "User " + socket.id
    });

    console.log("Client connected!");

    socket.on("Socket.Login.Attempt", nickname => {
        socket.Player = new Player(nickname);

        console.log(nickname + " is now part of the community!");

        socket.emit("Socket.Login.Successful", nickname);
    });

    socket.on("Socket.Tile.Clicked", (tile) => {
        let socketRoom = socket.Room;

        if (socketRoom.ActivePlayer != socket.Player) {
            socket.emit("Socket.Move.PlayerIsNotActive")
            return;
        }

        if (!socketRoom.makeMove(tile, socket.Player)) {
            socket.emit("Socket.Move.Illegal")
            return;
        }

        io.to(socketRoom.Guid).emit("Socket.Board.StateChanged", socketRoom.getGameState());

        if (!socketRoom.canOpponentMakeMove(socket.Player)) {
            console.log("Move not possible by opponent");

            if (!socketRoom.canPlayerMakeMove(socket.Player))
                endGame(socketRoom);
            else
                io.to(socketRoom.Guid).emit("Socket.Player.ActiveNotChanged");

            return;
        }

        passToken(socketRoom);
        io.to(socketRoom.Guid).emit("Socket.Player.SwitchActive");
    });

    socket.on("Socket.Game.CreateOrJoin", () => {
        let room = getFirstFreeRoom();

        if (!room) {
            room = new Room(defaultPlayerTime);
            rooms.push(room);
        }

        room.addPlayer(socket.Player);
        socket.join(room.Guid);
        socket.Room = room;

        if (room.Players.length == 2) {
            socket.Player.Color = TileColors.Black;
            socket.emit("Socket.Player.Set", 2)
            room.startGame();
            io.to(room.Guid).emit("Socket.Game.Start", { Players: room.Players, BoardState: room.getGameState(), TimePerPlayer: defaultPlayerTime });

            setTimeout(() => startCheckingIfTimesUp(room), defaultPlayerTime);

        } else {
            socket.Player.Color = TileColors.White;
            socket.emit("Socket.Player.Set", 1);
        }
    })
});

function endGame(room, intervalToBeCleared) {
    console.warn("-- Game ended --");
    
    room.endGame();
    let winner = room.getWinner();

    io.to(room.Guid).emit("Socket.Game.Finished", `The winner is ${winner.Name}!`);

    emptyRoom(room);
    deleteRoom(room);

    if (intervalToBeCleared)
        clearInterval(intervalToBeCleared);
}

function startCheckingIfTimesUp(room) {
    let timesUpInterval = setInterval(() => {
        if (room.getPlayerWhoseTimesUp())
            endGame(room, timesUpInterval);
    })
}

function emptyRoom(room) {
    Object.values(io.sockets.in(room.Guid).sockets).forEach(socket => {
        socket.leave(room);
    });
}

function deleteRoom(room) {
    let roomIndex = rooms.indexOf(room);

    if (roomIndex > -1)
        rooms.splice(roomIndex)
}

function getFirstFreeRoom() {
    return rooms.find(room => {
        return room.GameState == GameStates.NotStarted && room.Players.length < 2;
    });
}

function passToken(room) {
    room.switchActivePlayer();
}