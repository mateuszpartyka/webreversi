class Event {
    constructor(name, callback) {
        this.Name = name;
        this.Callbacks = [callback];
    }

    addCallback(callback) {
        this.Callbacks.push(callback);
    }
}

class Reactor {
    constructor() {
        this.Events = {};
    }

    subscribe(eventName, callback) {
        if (!this.Events.hasOwnProperty(eventName))
            this.Events[eventName] = new Event(eventName, callback);
        else
            this.Events[eventName].addCallback(callback);
    }

    publish(eventName, eventArgs) {
        if (!this.Events[eventName])
            return;
        
        this.Events[eventName].Callbacks.forEach((callback) => {
            callback(eventArgs);
            console.log("Triggered event", eventName)
        });
    }
}
