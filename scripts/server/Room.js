const uuidv4 = require('uuid/v4');
const GameStates = require('./GameStates');
const Game = require('./Game');
const Board = require('./Board');

class Room {
    constructor(defaultPlayerTime) {
        this.Guid = uuidv4();

        this.Players = [];
        this.ActivePlayer = null;
        this.DefaultPlayerTime = defaultPlayerTime;
        this.PlayersTime = [];
        this.CurrentPlayerInterval = null;

        this.Game = null;
        this.GameState = GameStates.NotStarted;
    }

    switchActivePlayer() {
        this.clearActivePlayerInterval();

        this.ActivePlayer = this.getOpponent(this.ActivePlayer);

        this.setActivePlayerInterval();
    }

    clearActivePlayerInterval() {
        clearInterval(this.CurrentPlayerInterval);
    }

    setActivePlayerInterval() {
        this.CurrentPlayerInterval = setInterval(() => this.subtractTimeFromActivePlayer(1000), 1000);
    }

    subtractTimeFromActivePlayer(time) {
        let indexOfActivePlayer = this.Players.indexOf(this.ActivePlayer);
        this.PlayersTime[indexOfActivePlayer] = this.PlayersTime[indexOfActivePlayer] - time;
    }

    makeMove(tile, player) {
        return this.Game.makeMove(tile, player.Color);
    }

    canPlayerMakeMove(player) {
        return this.Game.canPlayerMakeMove(player.Color);
    }

    canOpponentMakeMove(player) {
        return this.Game.canOpponentMakeMove(player.Color);
    }

    addPlayer(player) {
        this.Players.push(player);
    }

    startGame() {
        this.GameState = GameStates.Started;
        this.Game = new Game(new Board());

        this.PlayersTime[0] = this.DefaultPlayerTime;
        this.PlayersTime[1] = this.DefaultPlayerTime;

        this.ActivePlayer = this.Players[0];
        this.setActivePlayerInterval();
    }

    endGame() {
        this.GameState = GameStates.Ended;
    }

    getGameState() {
        return this.Game.getState();
    }

    getPlayerWhoseTimesUp() {
        let indexOfPlayerWhichTimesUp = this.PlayersTime.indexOf(0);
        return this.Players[indexOfPlayerWhichTimesUp];
    }

    getOpponent(player) {
        let indexOfPlayer = this.Players.indexOf(player);

        return this.Players[(indexOfPlayer + 1) % 2];
    }

    getWinner() {
        let gameState = this.getGameState();
        let winner = null;
        let playerWhoseTimesUp = this.getPlayerWhoseTimesUp();

        if (playerWhoseTimesUp)
            winner = this.getOpponent(playerWhoseTimesUp);
        else
            winner = gameState.Score[0] > gameState.Score[1] ? this.Players[0] : this.Players[1];

        return winner;
    }
}

module.exports = Room;