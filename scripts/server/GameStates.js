const GameStates = {
    NotStarted: 0,
    Started: 1,
    Ended: 2
}

module.exports = GameStates;