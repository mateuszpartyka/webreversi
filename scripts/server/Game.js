const TileColors = require('./TileColors');

class Game {
    constructor(board) {
        this.Board = board;
        this.Score = [2, 2];

        this.Board.Tiles[3][3].Color = TileColors.White;
        this.Board.Tiles[3][4].Color = TileColors.Black;
        this.Board.Tiles[4][4].Color = TileColors.White;
        this.Board.Tiles[4][3].Color = TileColors.Black;
    }

    canPlayerMakeMove(playerColor) {
        let freeTiles = [];
        
        this.Board.Tiles.forEach(row => {
            freeTiles = freeTiles.concat(row.filter(tile => tile.Color == null));
        });

        let possiblePlacesCount = 0;

        freeTiles.forEach(tile => {
            if (this.getTilesToMakeMove(tile, playerColor).length > 0)
                possiblePlacesCount++;    
        });

        return possiblePlacesCount > 0;
    }

    canOpponentMakeMove(playerColor) {
        let opponentColor = playerColor == TileColors.White ? TileColors.Black : TileColors.White;
        
        return this.canPlayerMakeMove(opponentColor);
    }

    getTilesToMakeMove(tile, color) {
        let tilesToChange = [];
        
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                tilesToChange = tilesToChange.concat(this.findTilesToFlip(tile, color, i, j));
            }
        }

        return tilesToChange;
    }

    makeMove(tile, color) {
        if (!this.isTileEmpty(tile.Row, tile.Column))
            return false;
            
        let tilesToChange = this.getTilesToMakeMove(tile, color);

        if (tilesToChange.length == 0)
            return false;

        tilesToChange.unshift(this.Board.Tiles[tile.Row][tile.Column]);
        this.changeTilesColor(color, tilesToChange)

        this.countScore();

        return true;
    }

    getState() {
        return {
            Score: this.Score,
            Tiles: this.Board.Tiles
        }
    }

    countScore() {
        this.Score = [0, 0];

        this.Board.Tiles.forEach(row => {
            row.forEach(tile => {
                if (tile.Color == TileColors.White)
                    this.Score[0]++;
                else if (tile.Color == TileColors.Black)
                    this.Score[1]++;
            })
        });
    }

    changeTilesColor(color, tiles) {
        tiles.forEach(tile => {
            tile.Color = color;
        });
    }

    findTilesToFlip(tile, color, rowDirection, columnDirection) {
        let tiles = [];
        let rowToCheck = tile.Row + rowDirection;
        let columnToCheck = tile.Column + columnDirection;

        while (this.isInRange(rowToCheck, 0, 8) && this.isInRange(columnToCheck, 0, 8) && !this.isTileEmpty(rowToCheck, columnToCheck)) {
            if (this.Board.Tiles[rowToCheck][columnToCheck].Color === color)
                return tiles;

            tiles.push(this.Board.Tiles[rowToCheck][columnToCheck]);
            rowToCheck += rowDirection;
            columnToCheck += columnDirection;
        }

        return [];
    }

    isInRange(value, min, max) {
        return value >= min && value < max;
    }

    isTileEmpty(row, column) {
        return !this.Board.Tiles[row][column].Color;
    }
}

module.exports = Game;