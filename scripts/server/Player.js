
const uuidv4 = require('uuid/v4');
class Player {
    constructor(name) {
        this.Guid = uuidv4();
        this.Name = name;
    }
}

module.exports = Player;