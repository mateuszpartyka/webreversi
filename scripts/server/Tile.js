class Tile {
    constructor(row, column, color) {
        this.Color = color;
        this.Row = row;
        this.Column = column;
    }
};

module.exports = Tile;