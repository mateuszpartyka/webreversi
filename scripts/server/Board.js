const Tile = require('./Tile');

class Board {
    constructor() {
        this.Tiles = [];

        this.initializeTiles();
    }

    initializeTiles() {
        for (let i = 0; i < 8; i++) {
            let row = [];

            for (let j = 0; j < 8; j++) {
                row.push(new Tile(i, j));
            }

            this.Tiles.push(row);
        }
    }
}

module.exports = Board;