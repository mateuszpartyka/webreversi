class Board {
    constructor(reactor, tileIdPrefix, classes) {
        this.Reactor = reactor;

        this.TileIdPrefix = tileIdPrefix;
        this.Classes = classes;

        this.TilesViews = [];
        this.Tiles = [];

        this.initializeTiles();
        this.drawTiles();

        this.Reactor.subscribe("Board.Initialize", boardState => this.initialize(boardState));
        this.Reactor.subscribe("Board.StateChanged", boardState => this.boardStateChanged(boardState));
    }

    initialize(boardState) {
        this.boardStateChanged(boardState);
        this.Reactor.publish("Board.Show");
    }

    boardStateChanged(boardState) {
        this.Reactor.publish("Score.Changed", boardState.Score);
        this.invalidate(boardState.Tiles);
    }

    invalidate(tiles) {
        this.Tiles = tiles;

        this.Tiles.forEach((row, rowIndex) => {
            row.forEach((tile, columnIndex) => {
                let tileView = this.TilesViews[rowIndex][columnIndex];

                Object.keys(this.Classes.TileColors).forEach(key => {
                    tileView.classList.toggle(this.Classes.TileColors[key], tile.Color === key);
                });
            })
        }, this);
    }

    initializeTiles() {
        for (var i = 0; i < 8; i++) {
            var row = [];

            for (var j = 0; j < 8; j++) {
                row.push(new Tile());
            }

            this.Tiles.push(row);
        }
    }

    drawTiles() {
        this.Tiles.forEach((row, rowIndex) => {
            let rowView = document.createElement("div");
            rowView.classList.add(this.Classes.Row);

            this.TilesViews.push([]);

            row.forEach((tile, columnIndex) => {
                let tileView = document.createElement("div");
                tileView.classList.add(this.Classes.Tile);
                tileView.id = this.TileIdPrefix + rowIndex + columnIndex;

                tile.Row = rowIndex;
                tile.Column = columnIndex;

                tileView.onclick = () => {
                    this.Reactor.publish("Tile.Clicked", tile);
                };

                // to draw actual circle
                tileView.appendChild(document.createElement("div"));

                this.TilesViews[rowIndex].push(tileView);

                rowView.appendChild(tileView);
            });

            this.Reactor.publish("Board.RowInitialized", rowView);
        });
    }
}