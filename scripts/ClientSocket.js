class ClientSocket {
    constructor (reactor) {
        this.Reactor = reactor;

        this.Socket = io.connect("http://localhost:1337");
        this.Username = "";

        this.setConnectionCallback()

        this.Reactor.subscribe("Tile.Clicked", (tile) => {
            this.Socket.emit("Socket.Tile.Clicked", tile);
        });
    }

    setConnectionCallback() {
        this.Socket.on("Socket.Connection.Successful", data => {
            this.Username = data.DefaultUsername;
            console.log(`Set default username to ${data.DefaultUsername}`);
        });

        this.Socket.on("Socket.Login.Successful", data => {
            this.Reactor.publish("Login.Successful", this.Username);
        });

        this.Socket.on("Socket.Game.Finished", endGameInfo => this.Reactor.publish("Game.Finished", endGameInfo));
        this.Socket.on("Socket.Game.Start", game => {
            this.Reactor.publish("Board.Initialize", game.BoardState);
            this.Reactor.publish("Players.Initialize", {Players: game.Players, TimePerPlayer: game.TimePerPlayer});
        });

        this.Socket.on("Socket.Board.StateChanged", boardState => this.Reactor.publish("Board.StateChanged", boardState));
        
        this.Socket.on("Socket.Player.Set", playerInfo => this.Reactor.publish("Player.SetAs", playerInfo));
        this.Socket.on("Socket.Player.SwitchActive", () => this.Reactor.publish("Player.SwitchActive"));
        this.Socket.on("Socket.Player.ActiveNotChanged", () => this.Reactor.publish("Player.ActiveNotChanged"));
    }

    login(username) {
        username = username.trim();
        this.Username = username || this.Username;
        this.Socket.emit("Socket.Login.Attempt", this.Username);
    }

    createOrJoinGame() {
        this.Socket.emit("Socket.Game.CreateOrJoin");
    }
}